import java.io.*;
class DecimalHex
{
    public static void main(String args[])throws IOException
    {
        BufferedReader br=new BufferedReader (new InputStreamReader(System.in));
        System.out.print("Ingrese un numero decimal : ");
        int n=Integer.parseInt(br.readLine());
 
        int r;
        String s=""; 
 
        //Arreglo de los digitos en hexadecimal
        char dig[]={'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
 
        while(n>0)
            {
                r=n%16; 
                s=dig[r]+s; 
                n=n/16;
            }
        System.out.println("Output = "+s);
    }
}